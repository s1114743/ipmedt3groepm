window.onload = () =>{
    const camera = document.getElementById("js--camera");
    const kruk = document.getElementById("js--kruk");
    const arrowBack = document.getElementById("js--arrowBack");
    const arrowNext = document.getElementById("js--arrowNext");
    const spelregelText = document.getElementById("js--spelregelText");
    let spelregelIndex = -1;
    const spelregels = ["Doel van het spel: Krijg 21 punten met je kaarten. \nAlle kaarten hebben de volgende waardes: \nAas: 1 of 11 punten, \nheer/vrouw/boer: 10, \nelk getal: de waarde die erop staat. \nKleuren en kaartsoort maakt niet uit.", "Als je je kaarten hebt heb je 2 keuzes. \n Pas: geen kaarten erbij en je wacht op de dealer. \n Hit: krijg een extra kaart, boven de 21 punten ben je af, onder de 21 punten mag je opnieuw kiezen.", "Je wint als je 21 punten hebt of meer punten dan de bank. \n Je bent af als je boven de 21 punten hebt(geldt ook voor de bank).", "Klik op de stoel om te beginnen"];

    const puntenSpeler = document.getElementById("js--puntenSpeler");
    const puntenMedespelerText = document.getElementById("js--puntenMedespelerText");
    const puntenDealerText = document.getElementById("js--puntenDealerText");
    const kaartendeck = document.getElementById("js--kaartendeck");
    const dealer = document.getElementById("js--dealer");

    const antwoordLinks = document.getElementById("js--antwoordLinks");
    const antwoordRechts = document.getElementById("js--antwoordRechts");
    const tekstAntwoordLinks = document.getElementById("js--tekstAntwoordLinks");
    const tekstAntwoordRechts = document.getElementById("js--tekstAntwoordRechts");
    const tekstGewonnen = document.getElementById("js--tekstGewonnen");

    const klaverAas = document.getElementById("js--klaver---Aas");
    const klaverKoning = document.getElementById("js--klaver---Koning");
    const klaverVrouw = document.getElementById("js--klaver---Vrouw");
    const klaverBoer = document.getElementById("js--klaver---Boer");
    const klaverTien = document.getElementById("js--klaver---Tien");
    const klaverNegen = document.getElementById("js--klaver---Negen");
    const klaverAcht = document.getElementById("js--klaver---Acht");
    const klaverZeven = document.getElementById("js--klaver---Zeven");
    const klaverZes = document.getElementById("js--klaver---Zes");
    const klaverVijf = document.getElementById("js--klaver---Vijf");
    const klaverVier = document.getElementById("js--klaver---Vier");
    const klaverDrie = document.getElementById("js--klaver---Drie");
    const klaverTwee = document.getElementById("js--klaver---Twee");

    const hartenAas = document.getElementById("js--harten---Aas");
    const hartenKoning = document.getElementById("js--harten---Koning");
    const hartenVrouw = document.getElementById("js--harten---Vrouw");
    const hartenBoer = document.getElementById("js--harten---Boer");
    const hartenTien = document.getElementById("js--harten---Tien");
    const hartenNegen = document.getElementById("js--harten---Negen");
    const hartenAcht = document.getElementById("js--harten---Acht");
    const hartenZeven = document.getElementById("js--harten---Zeven");
    const hartenZes = document.getElementById("js--harten---Zes");
    const hartenVijf = document.getElementById("js--harten---Vijf");
    const hartenVier = document.getElementById("js--harten---Vier");
    const hartenDrie = document.getElementById("js--harten---Drie");
    const hartenTwee = document.getElementById("js--harten---Twee");

    const ruitenAas = document.getElementById("js--ruiten---Aas");
    const ruitenKoning = document.getElementById("js--ruiten---Koning");
    const ruitenVrouw = document.getElementById("js--ruiten---Vrouw");
    const ruitenBoer = document.getElementById("js--ruiten---Boer");
    const ruitenTien = document.getElementById("js--ruiten---Tien");
    const ruitenNegen = document.getElementById("js--ruiten---Negen");
    const ruitenAcht = document.getElementById("js--ruiten---Acht");
    const ruitenZeven = document.getElementById("js--ruiten---Zeven");
    const ruitenZes = document.getElementById("js--ruiten---Zes");
    const ruitenVijf = document.getElementById("js--ruiten---Vijf");
    const ruitenVier = document.getElementById("js--ruiten---Vier");
    const ruitenDrie = document.getElementById("js--ruiten---Drie");
    const ruitenTwee = document.getElementById("js--ruiten---Twee");

    const schoppenAas = document.getElementById("js--schoppen---Aas");
    const schoppenKoning = document.getElementById("js--schoppen---Koning");
    const schoppenVrouw = document.getElementById("js--schoppen---Vrouw");
    const schoppenBoer = document.getElementById("js--schoppen---Boer");
    const schoppenTien = document.getElementById("js--schoppen---Tien");
    const schoppenNegen = document.getElementById("js--schoppen---Negen");
    const schoppenAcht = document.getElementById("js--schoppen---Acht");
    const schoppenZeven = document.getElementById("js--schoppen---Zeven");
    const schoppenZes = document.getElementById("js--schoppen---Zes");
    const schoppenVijf = document.getElementById("js--schoppen---Vijf");
    const schoppenVier = document.getElementById("js--schoppen---Vier");
    const schoppenDrie = document.getElementById("js--schoppen---Drie");
    const schoppenTwee = document.getElementById("js--schoppen---Twee");

    const kaartAnimatiePersoon1kaart1 = "property: position; dur: 1500; to:-1.35 0.9 -2.3;";
    const kaartAnimatiePersoon1kaart2 = "property: position; dur: 1500; to:-1.28 0.9 -2.18;";
    const kaartAnimatiePersoon1kaart3 = "property: position; dur: 1500; to:-1.17 0.9 -2;";

    const kaartAnimatiePersoon2kaart1 = "property: position; dur: 1500; to:0 0.9 -1.4;";
    const kaartAnimatiePersoon2kaart2 = "property: position; dur: 1500; to:0.16 0.9 -1.4;";
    const kaartAnimatieExtraKaart = "property: position; dur: 1500; to:0.34 0.9 -1.5;";

    const kaartAnimatieDealerkaart1 = "property: position; dur: 1500; to:0.25 0.9 -2.45;";
    const kaartAnimatieDealerkaart2 = "property: position; dur: 1500; to:0.4 0.9 -2.85;";
    const kaartAnimatieDealerKaart2Omgedraaid = "property: position; dur: 1500; to:0.4 0.9 -2.45;"

    const cameraAnimatieNaarKruk = "property: position; dur: 2000; to: 0 1.5 -1;";

    const randomKaartenArray = [klaverAas, klaverKoning, klaverVrouw, klaverBoer, klaverTien, klaverNegen, klaverAcht,
        klaverZeven, klaverZes, klaverVijf, klaverVier, klaverDrie, klaverTwee,
        hartenAas, hartenKoning, hartenVrouw, hartenBoer, hartenTien, hartenNegen, hartenAcht, hartenZeven, hartenZes,
        hartenVijf, hartenVier, hartenDrie, hartenTwee, ruitenAas, ruitenKoning, ruitenVrouw, ruitenBoer, ruitenTien, ruitenNegen, ruitenAcht, ruitenZeven, ruitenZes, ruitenVijf, ruitenVier, ruitenDrie, ruitenTwee, schoppenAas, schoppenKoning, schoppenVrouw, schoppenBoer, schoppenTien, schoppenNegen, schoppenAcht, schoppenZeven, schoppenZes, schoppenVijf, schoppenVier, schoppenDrie, schoppenTwee];

    let aantalPunten = [];
    let aantalPuntenMedespelerEen = [];
    let aantalPuntenDealer = [];
    let punten = 0;
    let puntenMedespeler = 0;
    let puntenDealer = 0;

    kaartendeck.onclick = (event) =>{
        randomKaartenUitdelen();
        setInterval(function() {
            randomKaartenUitdelen();
        },1500)
    }

    dealer.onclick = (event) =>{
        antwoordLinks.setAttribute("color", "red");
        antwoordRechts.setAttribute("color", "green");
        tekstAntwoordLinks.setAttribute("color", "black");
        tekstAntwoordRechts.setAttribute("color", "black");

        antwoordRechts.onclick = (event) =>{
            nieuweKaart();
            puntenTellenGebruiker();

        };
    }

    const randomKaartenUitdelen = ()=> {
        let kaart = [];

        if( randomKaartenArray.length > 46  ) {
            const randomIndex = Math.floor( Math.random() * randomKaartenArray.length );
            kaart = randomKaartenArray.splice( randomIndex, 1 )[0];

            if(randomKaartenArray.length == 51){
                kaart.setAttribute("animation", kaartAnimatiePersoon1kaart1);
                kaart.setAttribute("rotation", "-90 -60 0");
                aantalPuntenMedespelerEen.push(kaart.id.split("---").pop());
                puntenTellenMedespeler();
            } else if( randomKaartenArray.length == 50){
                kaart.setAttribute("animation", kaartAnimatiePersoon2kaart1);
                kaart.setAttribute("rotation", "-90 0 0");
                aantalPunten.push(kaart.id.split("---").pop() ); //hier staat het voorbeeld code en dit moet in de rest van de if statement worden aangepast
                puntenTellenGebruiker();
                console.log(aantalPunten);
            } else if(randomKaartenArray.length == 49){
                kaart.setAttribute("animation", kaartAnimatieDealerkaart1);
                kaart.setAttribute("rotation", "-90 0 0");
                aantalPuntenDealer.push(kaart.id.split("---").pop() );
                puntenTellenDealer();
            } else if(randomKaartenArray.length == 48){
                kaart.setAttribute("animation", kaartAnimatiePersoon1kaart2);
                kaart.setAttribute("rotation", "-90 -60 0");
                aantalPuntenMedespelerEen.push(kaart.id.split("---").pop() );
                puntenTellenMedespeler();
            }else if(randomKaartenArray.length == 47){
                kaart.setAttribute("animation", kaartAnimatiePersoon2kaart2);
                kaart.setAttribute("rotation", "-90 0 0");
                aantalPunten.push(kaart.id.split("---").pop() );
                puntenTellenGebruiker();
                console.log(aantalPunten);
            }else if(randomKaartenArray.length == 46){
                kaart.setAttribute("animation", kaartAnimatieDealerkaart2);
                kaart.setAttribute("rotation", "90 0 0");
               setTimeout(() => {
                    kaart.setAttribute("rotation", "-90 0 0");
                    kaart.setAttribute("animation", kaartAnimatieDealerKaart2Omgedraaid);
                    aantalPuntenDealer.push(kaart.id.split("---").pop() );
                puntenTellenDealer();
                }, 1500);
            }
        }
    }

    const nieuweKaart =()=>{
        let kaart = [];

        if( randomKaartenArray.length > 44 ) {
            const randomIndex = Math.floor( Math.random() * randomKaartenArray.length );
            kaart = randomKaartenArray.splice( randomIndex, 1 )[0];

            if(randomKaartenArray.length == 44){
                kaart.setAttribute("animation", kaartAnimatieExtraKaart);
                kaart.setAttribute("rotation", "-90 0 0");
            }
        }
        aantalPunten.push(kaart.id).split("---").pop();
    }


    const puntenTellenGebruiker = (event) =>{

        if(aantalPunten.includes("Aas") == true){
            punten += 11;
            aantalPunten.shift();

        }else if(aantalPunten.includes("Koning") == true || aantalPunten.includes("Vrouw") == true || aantalPunten.includes("Boer") ==true || aantalPunten.includes("Tien") == true ){
            punten += 10;
            aantalPunten.shift();
        }else if(aantalPunten.includes("Negen") == true ){
            punten += 9;
            aantalPunten.shift();
        }else if(aantalPunten.includes("Acht") == true ) {
            punten += 8;
            aantalPunten.shift();
        }else if(aantalPunten.includes("Zeven") == true) {
            punten += 7;
            aantalPunten.shift();
        }else if(aantalPunten.includes("Zes") == true ) {
            punten += 6;
            aantalPunten.shift();
        }else if(aantalPunten.includes("Vijf") == true ) {
            punten += 5;
            aantalPunten.shift();
        }else if(aantalPunten.includes("Vier") == true ) {
            punten += 4;
            aantalPunten.shift();
        }else if(aantalPunten.includes("Drie") == true ) {
            punten += 3;
            aantalPunten.shift();
        }else if(aantalPunten.includes("Twee") == true ) {
            punten += 2;
            aantalPunten.shift();
        }

        if(punten == 21){
            tekstGewonnen.setAttribute("color", "black");
        }

        if (punten > 21) {
            setTimeout(() => {
                tekstGewonnen.setAttribute("value", "Je hebt verloren!");
                tekstGewonnen.setAttribute("color", "black");
            }, 1500);
        }
        setTimeout(() => {
            puntenSpeler.setAttribute("value", punten)
        }, 1500);
        console.log(punten);
    }

    const puntenTellenMedespeler = (event) =>{

        if(aantalPuntenMedespelerEen.includes("Aas") == true){
            puntenMedespeler += 11;
            aantalPuntenMedespelerEen.shift();

        }else if(aantalPuntenMedespelerEen.includes("Koning") == true || aantalPuntenMedespelerEen.includes("Vrouw") == true || aantalPuntenMedespelerEen.includes("Boer") ==true || aantalPuntenMedespelerEen.includes("Tien") == true ){
            puntenMedespeler += 10;
            aantalPuntenMedespelerEen.shift();
        }else if(aantalPuntenMedespelerEen.includes("Negen") == true ){
            puntenMedespeler += 9;
            aantalPuntenMedespelerEen.shift();
        }else if(aantalPuntenMedespelerEen.includes("Acht") == true ) {
            puntenMedespeler += 8;
            aantalPuntenMedespelerEen.shift();
        }else if(aantalPuntenMedespelerEen.includes("Zeven") == true) {
            puntenMedespeler += 7;
            aantalPuntenMedespelerEen.shift();
        }else if(aantalPuntenMedespelerEen.includes("Zes") == true ) {
            puntenMedespeler += 6;
            aantalPuntenMedespelerEen.shift();
        }else if(aantalPuntenMedespelerEen.includes("Vijf") == true ) {
            puntenMedespeler += 5;
            aantalPuntenMedespelerEen.shift();
        }else if(aantalPuntenMedespelerEen.includes("Vier") == true ) {
            puntenMedespeler += 4;
            aantalPuntenMedespelerEen.shift();
        }else if(aantalPuntenMedespelerEen.includes("Drie") == true ) {
            puntenMedespeler += 3;
            aantalPuntenMedespelerEen.shift();
        }else if(aantalPuntenMedespelerEen.includes("Twee") == true ) {
            puntenMedespeler += 2;
            aantalPuntenMedespelerEen.shift();
        }

        if(puntenMedespeler == 21){
            tekstGewonnen.setAttribute("color", "black");
            tekstGewonnen.setAttribute("value", "Medespeler heeft gewonnen!");
        }

        if (puntenMedespeler > 21) {
            setTimeout(() => {
                puntenMedespelerText.setAttribute("value", "Medespeler heeft verloren!");
                puntenMedespelerText.setAttribute("color", "black");
                puntenMedespelerText.remove();
            }, 1750);

        }
        setTimeout(() => {
            puntenMedespelerText.setAttribute("value", puntenMedespeler);
        }, 1500);
        console.log(puntenMedespeler);
    }

    const puntenTellenDealer = (event) =>{

        if(aantalPuntenDealer.includes("Aas") == true){
            puntenDealer += 11;
            aantalPuntenDealer.shift();

        }else if(aantalPuntenDealer.includes("Koning") == true || aantalPuntenDealer.includes("Vrouw") == true || aantalPuntenDealer.includes("Boer") ==true || aantalPuntenDealer.includes("Tien") == true ){
            puntenDealer += 10;
            aantalPuntenDealer.shift();
        }else if(aantalPuntenDealer.includes("Negen") == true ){
            puntenDealer += 9;
            aantalPuntenDealer.shift();
        }else if(aantalPuntenDealer.includes("Acht") == true ) {
            puntenDealer += 8;
            aantalPuntenDealer.shift();
        }else if(aantalPuntenDealer.includes("Zeven") == true) {
            puntenDealer += 7;
            aantalPuntenDealer.shift();
        }else if(aantalPuntenDealer.includes("Zes") == true ) {
            puntenDealer += 6;
            aantalPuntenDealer.shift();
        }else if(aantalPuntenDealer.includes("Vijf") == true ) {
            puntenDealer += 5;
            aantalPuntenDealer.shift();
        }else if(aantalPuntenDealer.includes("Vier") == true ) {
            puntenDealer += 4;
            aantalPuntenDealer.shift();
        }else if(aantalPuntenDealer.includes("Drie") == true ) {
            puntenDealer += 3;
            aantalPuntenDealer.shift();
        }else if(aantalPuntenDealer.includes("Twee") == true ) {
            puntenDealer += 2;
            aantalPuntenDealer.shift();
        }

        if(puntenDealer == 21){
            tekstGewonnen.setAttribute("color", "black");
            tekstGewonnen.setAttribute("value", "Dealer heeft gewonnen!")
        }

        if (puntenDealer> 21) {
            setTimeout(() => {
                puntenDealerText.setAttribute("value", "Je hebt verloren!");
                puntenDealerText.setAttribute("color", "black");
                puntenDealerText.remove();
            }, 1750);

        }
        setTimeout(() => {
            puntenDealerText.setAttribute("value", puntenDealer);
        }, 1500);
        console.log(puntenDealer);
    }

    arrowNext.onclick = (event) => {
      spelregelIndex++;
      if (spelregelIndex < spelregels.length) {
        spelregelText.setAttribute("value", spelregels[spelregelIndex]);
      }
      if (spelregelIndex >= spelregels.length) {
        spelregelIndex = spelregels.length - 1;
      }
    }

    arrowBack.onclick = (event) => {
      spelregelIndex--;
      if (spelregelIndex >= 0) {
        spelregelText.setAttribute("value", spelregels[spelregelIndex]);
      }
      if (spelregelIndex < 0) {
        spelregelIndex = 0;
      }
    }

    kruk.onclick = (event) => {
      camera.setAttribute("animation", cameraAnimatieNaarKruk);
    }
}

window.onload = () => {
    const cursor = document.getElementById("js--cursor");
    const arrowNext = document.getElementById("js--arrowNext");
    const spelregelText = document.getElementById("js--spelregelText");
    
    let keuze = "";
    let keuzeAas = "";
    let deel2 = false;
    let deel3 = false;
    let spelregelIndex = -1;
    let spelregelIndex2 = -1;
    let spelregelIndex3 = -1;

    //deel 1
    const spelregels = ["Het is de bedoeling dat je met de uitgereikte kaarten van de dealer 21 punten behaald, of er zo dicht mogelijk bij zit. \n Als je meer dan 21 punten heb, ben je af en doe je niet meer mee." ,
    "Ik ga nu de kaarten uitdelen, elke kaart heeft een waarde: Aas: 1 of 11 punten. \n De heer/vrouw/boer en de 10 zijn 10 punten. \n elk ander getal: de waarde die erop staat.",
    "Nu ik de kaarten heb uitgedeeld, heb je 2 keuzes: \n pass of hit. \n Bij pass stop je en bij hit krijg je een extra kaart.",
    "Speler 1, welke keuze neem jij?", "Welke keuze neem jij? \n\n Ik zou hitten, want je hebt erg weinig punten." ];

    const spelregelsPass = ["Ik ben aan de beurt en leg nu mijn tweede kaart open.", "Nu ik minder dan 17 punten heb, moet ik als dealer een extra kaart pakken.",
    "Goed geprobeerd. Ik heb gewonnen, omdat ik de meeste punten heb. \n En omdat speler 1 en jij voor de optie pass zijn gegaan.",
    "Ben je klaar voor een tweede potje Blackjack?"];

    const spelregelsHit = ["Ik ben aan de beurt en leg nu mijn tweede kaart open.", "Nu ik minder dan 17 punten heb, moet ik als dealer een extra kaart pakken.",
    "Goed gedaan, je hebt gewonnen. \n\n Omdat ik meer dan 17 punten heb, mag ik geen extra kaart pakken."
    ,"Ben je klaar voor een tweede potje Blackjack?"];

    //deel 2
    const spelregelsDeel2 = ["Als je een hint wilt, klik dan op het vraagteken boven mij.",
    "Ik deel opnieuw de kaarten uit.", "Hoeveel punten wil je dat je Aas waard is? \n\n 1 of 11 punten?"];

    const spelregelsAas1 = ["Dat was niet de beste keuze die je kon maken.", "Speler 1 kiest voor Pass. \nWelke keuze ga je maken?\n\n Pass of hit?"];
    const spelregelAas11 = ["Je hebt de juiste beslissing genomen.", "Speler 1 kiest voor Pass. \nWelke keuze ga je maken?\n\n Pass of hit?"];

    //deel 3
    const spelregelsDeel3 = ["Ik ga voor de laatste keer de kaarten uitdelen.\nTijdens dit potje Blackjack zijn er geen hints aanwezig, maar wel een info button.\n Klik erop voor de spelregels.",
    "Als je dit wint, dan win je 2 keer je inzet!"];

    const hintTekst = ["Doel: krijg 21 punten of meer punten dan de dealer.\nAls je meer dan 21 punten heb ben je af en doe je niet meer mee.\nElke kaart heeft een waarde: Aas: 1 of 11 punten.\nDe heer/vrouw/boer en de 10 zijn 10 punten.\nElk ander getal: de waarde die erop staat."];

    const pijlRepeat = document.getElementById("js--pijlRepeat");
    const hintIcoon = document.getElementById("js--hintIcoon");
    const hintText = document.getElementById("js--hintText");
    const tekstwolkMedespeler = document.getElementById("js--tekstwolkMedespeler");
    const tekstwolkHint = document.getElementById("js--tekstwolkHint");
    const kruisHint = document.getElementById("js--kruisHint");

    const klaverAas = document.getElementById("js--klaverAas");
    const klaverBoer = document.getElementById("js--klaverBoer");
    const klaverNegen = document.getElementById("js--klaverNegen");
    const klaverZeven = document.getElementById("js--klaverZeven");
    const klaverDrie = document.getElementById("js--klaverDrie");
    const klaverTwee = document.getElementById("js--klaverTwee");

    const hartenVrouw = document.getElementById("js--hartenVrouw");
    const hartenBoer = document.getElementById("js--hartenBoer");
    const hartenAcht = document.getElementById("js--hartenAcht");
    const hartenVijf = document.getElementById("js--hartenVijf");
    const hartenTwee = document.getElementById("js--hartenTwee");

    const ruitenKoning = document.getElementById("js--ruitenKoning");
    const ruitenBoer = document.getElementById("js--ruitenBoer");
    const ruitenTien = document.getElementById("js--ruitenTien");
    const ruitenAcht = document.getElementById("js--ruitenAcht");
    const ruitenZeven = document.getElementById("js--ruitenZeven");
    const ruitenVijf = document.getElementById("js--ruitenVijf");
    const ruitenVier = document.getElementById("js--ruitenVier");

    const schoppenAas = document.getElementById("js--schoppenAas");
    const schoppenBoer = document.getElementById("js--schoppenBoer");
    const schoppenNegen = document.getElementById("js--schoppenNegen");
    const schoppenAcht = document.getElementById("js--schoppenAcht");
    const schoppenVier = document.getElementById("js--schoppenVier");
    const schoppenTwee = document.getElementById("js--schoppenTwee");

    const kaartAnimatiePersoon1kaart1 = "property: position; dur: 1500; to:-1.30 0.9 -1.3;"; 
    const kaartAnimatiePersoon1kaart2 = "property: position; dur: 1500; to:-1.02 0.9 -1.3;"; 

    const kaartAnimatiePersoon2kaart1 = "property: position; dur: 1500; to:0 0.9 -0.5;";
    const kaartAnimatiePersoon2kaart2 = "property: position; dur: 1500; to:0.16 0.9 -0.52;";
    const kaartAnimatiePersoon2kaart3 = "property: position; dur: 1500; to:0.34 0.9 -0.5;";

    //deel 1
    const kaartAnimatieDealerkaart1 = "property: position; dur: 1500; to:0.25 0.9 -1.3;"; 
    const kaartAnimatieDealerkaart2 = "property: position; dur: 1500; from:0.25 0.9 -1.3; to: 0.53 0.9 -1.3;"; 
    const kaartAnimatieDealerKaart2Omgedraaid = "property: position; dur: 0; from: 0.43 0.9 -1.77; to:0.4 0.9 -1.3;"; 
    const kaartAnimatieDealerKaart3 = "property: position; dur: 1500; to:0.55 0.9 -1.3;"

    //deel 2
    const kaartAnimatieDealerKaart2Deel2 = "property: position; dur: 1500; from: 0.2 0.9 -1.3; to:0.55 0.9 -1.0;"; 
    const kaartAnimatieDealerKaart2OmgedraaidDeel2 = "property: position; dur: 0; from: 0.2 0.9 -1.3; to:0.45 0.85 -1.3;"; 
    const kaartAnimatieDealerKaart3Deel2 = "property: position; dur: 1500; from: 0.2 0.9 -1.3; to:0.6 0.85 -1.2;"; 

    //deel 3
    const kaartAnimatieDealerKaart2Deel3 = "property: position; dur: 1500; from: 0.2 0.9 -1.3; to:0.62 0.9 -1.3;"; 

    const puntenSpelerText = document.getElementById("js--puntenSpelerText");
    const puntenMedespelerText = document.getElementById("js--puntenMedespelerText");
    const puntenDealerText = document.getElementById("js--puntenDealerText");

    const keuzeMoment = document.getElementById("js--keuzeMoment");

    const circlePass = document.getElementById("js--circlePass");
    const circlePassText = document.getElementById("js--circlePassText");
    const circleHit = document.getElementById("js--circleHit");
    const circleHitText = document.getElementById("js--circleHitText");
    const vraagteken = document.getElementById("js--vraagteken");

    const fishesZwartStapel = document.getElementById("js--fishesZwartStapel");
    const fishesBlauwStapel = document.getElementById("js--fishesBlauwStapel");
    const fishesWitStapel = document.getElementById("js--fishesWitStapel");
    const fishesGroenStapel = document.getElementById("js--fishesGroenStapel");

    const schuifStok = document.getElementById("js--stok");

    const animatie_fishesNaarMidden1 = "property: position; dur: 1500; to: 0.17 0.83 -1.2";
    const animatie_fishesNaarMidden2 = "property: position; dur: 1500; to: 0 0.83 -1.2";
    const animatie_fishesNaarMidden3 = "property: position; dur: 1500; to: -.17 0.83 -1.2";
    const animatie_fishesNaarMidden4 = "property: position; dur: 1500; to: -.34 0.83 -1.2";

    const animatie_fishesNaarSpeler1 = "property: position; dur: 1500; to: -.3 0.83 -0.60";
    const animatie_fishesNaarSpeler2 = "property: position; dur: 1500; to: -.45 0.83 -0.65";
    const animatie_fishesNaarSpeler3 = "property: position; dur: 1500; to: -.6 0.83 -0.7";
    const animatie_fishesNaarSpeler4 = "property: position; dur: 1500; to: -.75 0.83 -0.75";

    const animatie_schuifStok = "property: position; dur: 1500; from: 0 0.9 -2.0; to: -.5 0.9 -1.4";
    const animatie_schuifStok2 = "property: position; dur: 1500; from: -.5 0.9 -1.4; to: 1 0.9 -1.7";

    hintIcoon.setAttribute("scale", "0 0 0");
    schuifStok.setAttribute("scale", "0 0 0");
    fishesBlauwStapel.setAttribute("scale", "0 0 0");
    fishesGroenStapel.setAttribute("scale", "0 0 0");
    fishesWitStapel.setAttribute("scale", "0 0 0");
    fishesZwartStapel.setAttribute("scale", "0 0 0");

    arrowNext.onclick = (event) => {
        cursorKleur();

        spelregelIndex++;
        if (spelregelIndex < spelregels.length) {
            spelregelText.setAttribute("value", spelregels[spelregelIndex]);
            switch(spelregelIndex){
                case 1:
                    arrowNext.setAttribute("scale", "0 0 0");
                    setTimeout( () =>{
                        kaartUitdelen();
                    },5000); 
                    break;
                case 3:
                    tekstwolkMedespeler.setAttribute("scale", "0.2 0.2 0.2");
                    break;
                case 4:
                    kaartUitdelen2();
                    arrowNext.setAttribute("scale", "0 0 0");
                    break;
                case 5:
                    ruitenZeven.setAttribute("rotation", "0 -5 0");
                    ruitenZeven.setAttribute("animation", kaartAnimatieDealerKaart2Omgedraaid);
                    puntenDealerText.setAttribute("value", "16");
                    if (keuze == "pass") {
                        spelregels.push(spelregelsPass[1]);
                    }
                    else if (keuze == "hit") {
                        spelregels.push(spelregelsHit[1]);
                    }

                    break;
                case 6:
                    if(keuze == "pass"){
                        arrowNext.setAttribute("scale", "0 0 0");
                        schoppenVier.setAttribute("animation", kaartAnimatieDealerKaart3);
                        schoppenVier.setAttribute("rotation", "0 -5 0");
                        setTimeout( () =>{
                            puntenDealerText.setAttribute("value", "20");
                            arrowNext.setAttribute("scale", "0.25 0.20 0.25");
                        },1500);
                        spelregels.push(spelregelsPass[2]);

                    }
                    else if(keuze == "hit"){
                        arrowNext.setAttribute("scale", "0 0 0");
                        setTimeout ( () =>{
                            klaverTwee.setAttribute("animation", kaartAnimatieDealerKaart3);
                            klaverTwee.setAttribute("rotation", "0 -5 0");
                            setTimeout( () =>{
                                puntenDealerText.setAttribute("value", "18");
                                spelregels.push(spelregelsHit[2]);
                                arrowNext.setAttribute("scale", "0.25 0.20 0.25");
                            },1000);
                        },500);
                    }
                    break;
                case 7:
                    if(keuze == "pass"){
                        kaartenTerugNemen1();
                        schoppenVier.setAttribute("position", "0 0.9 -1.48");
                    }
                    else if(keuze == "hit"){
                        kaartenTerugNemen1();
                        klaverTwee.setAttribute("position", "0 0.9 -1.48");
                        ruitenVijf.setAttribute("position", "0 0.9 -1.48");
                    }
                    deel2 = true;
                    break;
            }
        }
        else if(deel2 == true){
            spelregelIndex2++;
            spelregelText.setAttribute("value", spelregelsDeel2[spelregelIndex2]);
            switch(spelregelIndex2){
                case 0:
                    vraagteken.setAttribute("scale", "1 1 1");
                    break;
                case 1:
                    vraagteken.setAttribute("scale", "0 0 0");
                    arrowNext.setAttribute("scale", "0 0 0");
                    setTimeout( () =>{
                        kaartUitdelenDeel2();
                    },1000);
                    break;
                case 2:
                    vraagteken.setAttribute("scale", "1 1 1");
                    kaartKeuzeAas();
                    break;
                case 3:
                    if(keuzeAas == 1){
                        spelregelsDeel2.push(spelregelsAas1[1]);
                    }
                    else if(keuzeAas == 11){
                        spelregelsDeel2.push(spelregelAas11[1]);
                    }
                    break;
                case 4:
                    kaartUitDelenAasDeel2();
                    break;
                case 5:
                    ruitenAcht.setAttribute("rotation", "0 -5 0");
                    ruitenAcht.setAttribute("animation", kaartAnimatieDealerKaart2OmgedraaidDeel2);
                    puntenDealerText.setAttribute("value", "11");
                    spelregelText.setAttribute("value", "Omdat ik lager dan 17 punten heb, moet ik een extra kaart pakken.");
                    break;
                case 6:
                    arrowNext.setAttribute("scale", "0 0 0");
                    schoppenAcht.setAttribute("rotation", "0 -5 0");
                    schoppenAcht.setAttribute("animation", kaartAnimatieDealerKaart3Deel2);
                    setTimeout( () =>{
                        arrowNext.setAttribute("scale", "0.25 0.20 0.25");
                        puntenDealerText.setAttribute("value", "19");
                    },1500);
                    spelregelText.setAttribute("value", "Ik heb meer punten dan jij, dus je hebt verloren.\n\nKom we gaan een derde potje spelen, maar dan zonder hints.");

                    break;
                case 7:
                    kaartenTerugnemen2();
                    deel3 = true;
                    break;
            }
            if(deel3 == true){        
                hintIcoon.setAttribute("scale", "1 1 1");     
                spelregelIndex3++;
                spelregelText.setAttribute("value", spelregelsDeel3[spelregelIndex3]);
    
                switch(spelregelIndex3){
                    case 1:
                        fishesAnimatieInzetten();
                        arrowNext.setAttribute("scale", "0 0 0");
                        setTimeout( () => {
                            kaartUitdelenDeel3();
                        }, 1500);
                        
                        break;
                    case 2:
                        klaverNegen.setAttribute("rotation", "0 -5 0");
                        klaverNegen.setAttribute("animation", kaartAnimatieDealerKaart2Omgedraaid);
                        puntenDealerText.setAttribute("value", "19");
                        spelregelText.setAttribute("value", "Je hebt gewonnen! Gefeliciteerd! \nJe wint nu 2 keer je inzet."); 
                        arrowNext.setAttribute("scale", "value");
                        fishesAnimatieGewonnen();                       
                        break;
                }
            }
        }
        audioplay(spelregelText);
    }

    const fishesAnimatieGewonnen = () =>{
        schuifStok.setAttribute("scale", "0.07 0.07 0.07");
        fishesBlauwStapel.setAttribute("scale", "1 1 1");
        fishesGroenStapel.setAttribute("scale", "1 1 1");
        fishesWitStapel.setAttribute("scale", "1 1 1");
        fishesZwartStapel.setAttribute("scale", "1 1 1");

        fishesGroenStapel.setAttribute("animation", animatie_fishesNaarSpeler1);
        fishesZwartStapel.setAttribute("animation", animatie_fishesNaarSpeler2);
        fishesBlauwStapel.setAttribute("animation", animatie_fishesNaarSpeler3);
        fishesWitStapel.setAttribute("animation", animatie_fishesNaarSpeler4);
        schuifStok.setAttribute("animation", animatie_schuifStok);

        setTimeout( () =>{
            schuifStok.setAttribute("animation", animatie_schuifStok2);
        },2500);
    }

    const fishesAnimatieInzetten = () =>{
        schuifStok.setAttribute("scale", "0.07 0.07 0.07");
        fishesBlauwStapel.setAttribute("scale", "1 1 1");
        fishesGroenStapel.setAttribute("scale", "1 1 1");
        fishesWitStapel.setAttribute("scale", "1 1 1");
        fishesZwartStapel.setAttribute("scale", "1 1 1");

        fishesGroenStapel.setAttribute("animation", animatie_fishesNaarMidden1);
        fishesZwartStapel.setAttribute("animation", animatie_fishesNaarMidden2);
        fishesBlauwStapel.setAttribute("animation", animatie_fishesNaarMidden3);
        fishesWitStapel.setAttribute("animation", animatie_fishesNaarMidden4);
    }

    const kaartUitdelen = () =>{
        //medespeler kaart 1
        hartenAcht.setAttribute("animation", kaartAnimatiePersoon1kaart1);
        hartenAcht.setAttribute("rotation", "0 20 0");
        setTimeout( () => {
            // gebruiker kaart 1
            puntenMedespelerText.setAttribute("value", "8");
            klaverBoer.setAttribute("animation", kaartAnimatiePersoon2kaart1);
            klaverBoer.setAttribute("rotation", "-90 0 0");
            setTimeout( () =>{
                //dealer kaart 1
                puntenSpelerText.setAttribute("value", "10");
                schoppenNegen.setAttribute("animation", kaartAnimatieDealerkaart1);
                schoppenNegen.setAttribute("rotation", "0 -5 0");
                setTimeout( () => {
                    // medespeler kaart 2
                    puntenDealerText.setAttribute("value", "9");
                    ruitenTien.setAttribute("animation", kaartAnimatiePersoon1kaart2);
                    ruitenTien.setAttribute("rotation", "0 20 0");
                    setTimeout( () => {
                        //gebruiker kaart 2
                        puntenMedespelerText.setAttribute("value", "18");
                        hartenVijf.setAttribute("animation", kaartAnimatiePersoon2kaart2);
                        hartenVijf.setAttribute("rotation", "-90 0 0");
                        setTimeout( () => {
                            //dealer kaart 2
                            puntenSpelerText.setAttribute("value", "15");
                            ruitenZeven.setAttribute("animation", kaartAnimatieDealerkaart2);
                            ruitenZeven.setAttribute("rotation", "0 175 0");
                            setTimeout( () => {
                                arrowNext.setAttribute("scale", "0.25 0.20 0.25");

                            },1500); 
                        },1500);  
                    },1500);    
                },1500); 
            }, 1500);
        },1500);
    }

    const kaartUitdelen2 = () =>{
        tekstwolkMedespeler.setAttribute('scale' , "0 0 0");
        keuzeMoment.setAttribute("scale", "1 1 1");

        circlePass.onclick = () =>{
            cursorKleur();
            spelregelText.setAttribute("value", "Ik zou dat niet hebben gedaan. \nDe kans dat mijn tweede kaart een hoog punten aantal heeft, is zeer aanwezig");
            arrowNext.setAttribute("scale", "0.25 0.20 0.25");
            spelregels.push(spelregelsPass[0]);
            keuzeMoment.setAttribute("scale", "0 0 0");
            keuze = "pass";
            audioplay(spelregelText);
        }

        circleHit.onclick = () =>{
            cursorKleur();
            spelregelText.setAttribute("value", "Je hebt de juiste beslissing genomen.");
            arrowNext.setAttribute("scale", "0.25 0.20 0.25");
            spelregels.push(spelregelsHit[0]);
            keuzeMoment.setAttribute("scale", "0 0 0");
            keuze = 'hit';
            audioplay(spelregelText);
            if(keuze == 'hit'){
                ruitenVijf.setAttribute("animation", kaartAnimatiePersoon2kaart3);
                ruitenVijf.setAttribute("rotation", "-90 0 0");
                setTimeout( () =>{
                    puntenSpelerText.setAttribute("value", "20");
                },1500);
            }
        }
    }

    const kaartenTerugNemen1 = () =>{
        schoppenVier.setAttribute("rotation", "-90 0 0");
        klaverBoer.setAttribute("position", "0 0.9 -1.48");
        hartenAcht.setAttribute("position", "0 0.9 -1.48");
        hartenAcht.setAttribute("rotation", "-90 0 0");
        hartenVijf.setAttribute("position", "0 0.9 -1.48");
        ruitenZeven.setAttribute("position", "0 0.9 -1.48");
        ruitenZeven.setAttribute("rotation", "-90 0 0");
        ruitenTien.setAttribute("position", "0 0.9 -1.48");
        ruitenTien.setAttribute("rotation", "-90 0 0");
        schoppenNegen.setAttribute("position", "0 0.9 -1.48");
        schoppenNegen.setAttribute("rotation", "-90 0 0");
        klaverTwee.setAttribute("position", "0 0.9 -1.48");
        klaverTwee.setAttribute("rotation", "-90 0 0");
        puntenMedespelerText.setAttribute("value", " ");
        puntenSpelerText.setAttribute("value", " ");
        puntenDealerText.setAttribute("value", " ");
    }

    const kaartenTerugnemen2 = () => {
        hartenBoer.setAttribute("position", "0 0.9 -1.48");
        hartenBoer.setAttribute("rotation", "-90 0 0"); //(rotatie)
        schoppenAas.setAttribute("position", "0 0.9 -1.48");
        klaverDrie.setAttribute("position", "0 0.9 -1.48");
        hartenVrouw.setAttribute("position", "0 0.9 -1.48");
        hartenVrouw.setAttribute("rotation", "-90 0 0"); //(rotatie)
        klaverZeven.setAttribute("position", "0 0.9 -1.48");
        ruitenAcht.setAttribute("position", "0 0.9 -1.48");
        ruitenAcht.setAttribute("rotation", "-90 0 0"); //(rotatie)
        schoppenBoer.setAttribute("position", "0 0.9 -1.48");
        ruitenVier.setAttribute("position", "0 0.9 -1.48");
        schoppenAcht.setAttribute("position", "0 0.9 -1.48");
        schoppenAcht.setAttribute("rotation", "-90 0 0"); //(rotatie)
        klaverDrie.setAttribute("rotation", "-90 0 0"); //(rotatie)
        puntenMedespelerText.setAttribute("value", " ");
        puntenSpelerText.setAttribute("value", " ");
        puntenDealerText.setAttribute("value", " ");
    }

    const kaartUitdelenDeel2 = () =>{
        //medespeler kaart 1
        hartenBoer.setAttribute("animation", kaartAnimatiePersoon1kaart1);
        hartenBoer.setAttribute("rotation", "0 20 0");
        setTimeout( () => {
            // gebruiker kaart 1
            puntenMedespelerText.setAttribute("value", "10");
            schoppenAas.setAttribute("animation", kaartAnimatiePersoon2kaart1);
            schoppenAas.setAttribute("rotation", "-90 0 0");
            setTimeout( () =>{
                //dealer kaart 1
                puntenSpelerText.setAttribute("value", " ");
                klaverDrie.setAttribute("animation", kaartAnimatieDealerkaart1);
                klaverDrie.setAttribute("rotation", "0 -5 0");
                setTimeout( () => {
                    // medespeler kaart 2
                    puntenDealerText.setAttribute("value", "3");
                    hartenVrouw.setAttribute("animation", kaartAnimatiePersoon1kaart2);
                    hartenVrouw.setAttribute("rotation", "0 20 0");
                    setTimeout( () => {
                        //gebruiker kaart 2
                        puntenMedespelerText.setAttribute("value", "20");
                        klaverZeven.setAttribute("animation", kaartAnimatiePersoon2kaart2);
                        klaverZeven.setAttribute("rotation", "-90 0 0");
                        setTimeout( () => {
                            //dealer kaart 2
                            puntenSpelerText.setAttribute("value", "7");
                            ruitenAcht.setAttribute("animation", kaartAnimatieDealerKaart2Deel2);
                            ruitenAcht.setAttribute("rotation", "0 175 0");
                            setTimeout( () => {
                                arrowNext.setAttribute("scale", "0.25 0.20 0.25");

                            },1500); 
                        },1500);  
                    },1500); 
                },1500);  
            }, 1500);
        },1500);
    }

    const kaartUitdelenDeel3 = () =>{
        //medespeler kaart 1
        hartenTwee.setAttribute("animation", kaartAnimatiePersoon1kaart1);
        hartenTwee.setAttribute("rotation", "0 20 0");
        setTimeout( () => {
            // gebruiker kaart 1
            puntenMedespelerText.setAttribute("value", "2");
            ruitenKoning.setAttribute("animation", kaartAnimatiePersoon2kaart1);
            ruitenKoning.setAttribute("rotation", "-90 0 0");
            setTimeout( () =>{
                //dealer kaart 1
                puntenSpelerText.setAttribute("value", "10");
                ruitenBoer.setAttribute("animation", kaartAnimatieDealerkaart1);
                ruitenBoer.setAttribute("rotation", "0 -5 0");
                setTimeout( () => {
                    // medespeler kaart 2
                    puntenDealerText.setAttribute("value", "10");
                    schoppenTwee.setAttribute("animation", kaartAnimatiePersoon1kaart2);
                    schoppenTwee.setAttribute("rotation", "0 20 0");
                    setTimeout( () => {
                        //gebruiker kaart 2
                        puntenMedespelerText.setAttribute("value", "4");
                        klaverAas.setAttribute("animation", kaartAnimatiePersoon2kaart2);
                        klaverAas.setAttribute("rotation", "-90 0 0");
                        setTimeout( () => {
                            //dealer kaart 2
                            puntenSpelerText.setAttribute("value", "21");
                            klaverNegen.setAttribute("animation", kaartAnimatieDealerKaart2Deel3);
                            klaverNegen.setAttribute("rotation", "0 175 0");
                            setTimeout( () => {
                                arrowNext.setAttribute("scale", "0.25 0.20 0.25");

                            },1500); 
                        },1500); 
                    },1500);  
                },1500); 
            }, 1500);
        },1500);
    }

    let hint = false;
    const kaartKeuzeAas = ()=>{
        keuzeMoment.setAttribute("scale", "1 1 1");
        circlePassText.setAttribute("value", "1");
        circlePassText.setAttribute("position", "-0.07 -0.15 0");
        circleHitText.setAttribute("value", "11");
        circleHitText.setAttribute("position", "-0.10 -0.15 0");
        arrowNext.setAttribute("scale", "0 0 0");

        //kiest aas als 1 punt
        circlePass.onclick = () =>{
            cursorKleur();
            puntenSpelerText.setAttribute("value", "8");
            keuzeMoment.setAttribute("scale", "0 0 0");
            vraagteken.setAttribute("scale", "0 0 0");
            spelregelText.setAttribute("value", "Je hebt gekozen voor Aas is 1 punt.");
            spelregelsDeel2.push(spelregelsAas1[0]);

            arrowNext.setAttribute("scale", "0.25 0.20 0.25");
            hint = true;
            keuzeAas = 1;
            audioplay(spelregelText);
        }
        // kiest aas als 11 punten
        circleHit.onclick = () =>{
            cursorKleur();
            puntenSpelerText.setAttribute("value", "18");
            keuzeMoment.setAttribute("scale", "0 0 0");
            vraagteken.setAttribute("scale", "0 0 0");
            spelregelText.setAttribute("value", "Je hebt gekozen voor Aas is 11 punten.");
            arrowNext.setAttribute("scale", "0.25 0.20 0.25");
            spelregelsDeel2.push(spelregelAas11[0]);
            hint = true;
            keuzeAas = 11;
            audioplay(spelregelText);
        }

        vraagteken.onclick = () =>{
            cursorKleur();
            spelregelText.setAttribute("value", "Hint: \nAls je voor 1 kiest is je kans voor slagen minder groot, dan als je voor 11 kiest.");
            if(hint == true){
                vraagteken.setAttribute("scale", "0 0 0 ");
                spelregelIndex2++;
            }
            audioplay(spelregelText);
        }
    }


    const kaartUitDelenAasDeel2 = () =>{
        arrowNext.setAttribute("scale", "0 0 0");
        keuzeMoment.setAttribute("scale", " 1 1 1");
        circlePassText.setAttribute("value", "Pass");
        circlePassText.setAttribute("position", "-0.25 -0.15 0");
        circleHitText.setAttribute("value", "Hit");
        circleHitText.setAttribute("position", "-0.17 -0.15 0");
        vraagteken.setAttribute("scale", "1 1 1");
        tekstwolkMedespeler.setAttribute("scale" , "0.2 0.2 0.2");

        circlePass.onclick = () =>{
            cursorKleur();
            tekstwolkMedespeler.setAttribute("scale" , "0 0 0");
            if(keuzeAas == 1){
                spelregelText.setAttribute("value", "Ik zou dat niet hebben gedaan.\nDe kans dat ik meer punten krijg dan jij, is zeer groot.");
                arrowNext.setAttribute("scale", "0.25 0.20 0.25");
                keuzeMoment.setAttribute("scale", "0 0 0");
                vraagteken.setAttribute("scale", "0 0 0");
                hint = true;
                keuze = "pass";
            }
            else if (keuzeAas == 11){
                cursorKleur();
                spelregelText.setAttribute("value", "Je hebt de juiste beslissing genomen.");
                arrowNext.setAttribute("scale", "0.25 0.20 0.25");
                keuzeMoment.setAttribute("scale", "0 0 0");
                vraagteken.setAttribute("scale", "0 0 0");
                hint = true;
                keuze = "pass";
            }
            audioplay(spelregelText);
        }

        circleHit.onclick = () => {
            cursorKleur();
            tekstwolkMedespeler.setAttribute("scale" , "0 0 0");
            if(keuzeAas == 1){
                spelregelText.setAttribute("value", "Je hebt de juiste beslissing genomen.");
                arrowNext.setAttribute("scale", "0.25 0.20 0.25");
                keuzeMoment.setAttribute("scale", "0 0 0");
                vraagteken.setAttribute("scale", "0 0 0");
                keuze = "hit";
                hint = true;
                if(keuze == "hit"){
                    schoppenBoer.setAttribute("animation", kaartAnimatiePersoon2kaart3);
                    schoppenBoer.setAttribute("rotation", "-90 0 0");
                    setTimeout( () =>{
                        puntenSpelerText.setAttribute("value", "18");
                        spelregelText.setAttribute("value", "Dat is een mooie kaart!");
                        audioplay(spelregelText);
                    },2500);
                }

            }
            else if(keuzeAas == 11){
                arrowNext.setAttribute("scale", "0.25 0.20 0.25");
                keuzeMoment.setAttribute("scale", "0 0 0");
                vraagteken.setAttribute("scale", "0 0 0");
                if(keuze == "hit"){
                    ruitenVier.setAttribute("animation", kaartAnimatiePersoon2kaart3);
                    ruitenVier.setAttribute("rotation", "-90 0 0");
                    setTimeout( () =>{
                        puntenSpelerText.setAttribute("value", "22");
                    },1500);
                    spelregelText.setAttribute("value", "Je hebt nu meer dan 21 punten, dus je hebt verloren.");
                }
                hint = true;
                keuze = "hit";

            }
            audioplay(spelregelText);
        }

        vraagteken.onclick = () =>{
            cursorKleur();
            if (keuzeAas == 1){
                spelregelText.setAttribute("value", "Hint: \nAls je voor Pass gaat dan heb je sowieso verloren.");
                if(hint == true){
                    vraagteken.setAttribute("scale", "0 0 0 ");
                    spelregelIndex2++;
                }
                audioplay(spelregelText);
            }
            else if(keuzeAas == 11){
                spelregelText.setAttribute("value", "Hint: \nAls je voor Hit kiest, dan is de kans groot dat je over de 21 punten gaat.");
                if(hint == true){
                    vraagteken.setAttribute("scale", "0 0 0 ");
                    spelregelIndex2++;
                }
                audioplay(spelregelText);
            }

        }
    }

    const audioplay = (place) => {
      responsiveVoice.speak(place.getAttribute("value"), "Dutch Male");
    }

    hintIcoon.onclick = () => {
      tekstwolkHint.setAttribute("scale", "0.4 0.4 0.4");
      //hintText.setAttribute("value", "Doel: krijg 21 punten of meer punten dan de dealer. \n Als je meer dan 21 punten heb ben je af en doe je niet meer mee.\n Elke kaart heeft een waarde: Aas: 1 of 11 punten. \n De heer/vrouw/boer en de 10 zijn 10 punten. \n elk ander getal: de waarde die erop staat.");
      hintText.setAttribute("value", hintTekst);
      audioplay(hintText);
      cursorKleur();
    }

    kruisHint.onclick = () => {
      tekstwolkHint.setAttribute("scale", "0 0 0");
      cursorKleur();
    }

    pijlRepeat.onclick = () =>{
        audioplay(spelregelText);
        cursorKleur();
    }

    const cursorKleur = () =>{
        cursor.setAttribute("color", "white");
        setTimeout( () =>{
            cursor.setAttribute("color", "#545454");
        },500);
    }
}
